module modernc.org/db

go 1.21

require (
	modernc.org/file v1.0.9
	modernc.org/internal v1.1.1
	modernc.org/mathutil v1.7.1
	modernc.org/strutil v1.2.1
)

require (
	github.com/edsrzf/mmap-go v1.2.0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/sys v0.28.0 // indirect
	modernc.org/fileutil v1.3.0 // indirect
)
